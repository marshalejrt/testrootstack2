# Prueba Remota para Rootstack #

Test práctico desarrollado en PHP / Javascript / Html utilizando la metodología MVC

### De qué se trata? ###

* Encontrará Funciones get y solicitudes a servidores externos
* Manejo de Arrays, Strings, Ordenamiento, entre otros.
* Utilización de librerias Bootstrap / Jquery / Isotope


### Cómo hacer Funcionar el Proyecto? ###

## Setup Inicial

### En primer lugar debemos ubicarnos en nuestra var/wwww ó public/htmnl del servidor y clonar el repositorio

> Creamos una carpeta en donde se alojarán todos los proyectos que deseamos estén disponibles en el servidor Homestead.
> Dicha carpeta la llamaremos ~/rootstack

```
cd ~
mkdir rootstack
```

#### Clonamos el repositorio
Descargamos el código del proyecto desde el repositorio.
No olvidarse reemplazar __USERNAME__ por su nombre de usuario de BitBucket.

```
cd ~/rootstack
git clone https://USERNAME@bitbucket.org/marshalejrt/testrootstack2.git
cd ~/testrootstack2/App
```

### Ir a nuestro Navegador y ubicarnos en la siguiente dirección: ###
```
http://localhost/rootstack/testrootstack2/App/
```

### Creado Por: ###

* Ing. Efrain Rodriguez para Rootstack
