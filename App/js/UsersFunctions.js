$(document).ready(function () {
    $('.sort-by-button-group').on( 'click', 'button', function() {
        $('.sort-by-button-group button').css('color','white');
        $(this).css('color','orange');
        var sortValue = $(this).attr('data-sort-by');
        $('.grid').isotope({ sortBy: sortValue });
    });

    ordenarUsuarios();
});

function ordenarUsuarios(){
    $('.grid').isotope({
        // options

        getSortData: {
            name: '.name', // text from querySelector
            last_name:'.last_name'
        },
        sortBy: 'name'
    });
    $('.sort-by-button-group .name-order').css('color','orange');
}

function validarEdad()    {
    if ($('#Edad').val()==''){
        alert("Debe ingresar una Edad Correcta, verifique e intente nuevamente.");
        $('#Edad').focus();
        return false;
    }else{
        $('#buscando').show();
    }
}

function validarPasajeros()    {
    if ($('#Pasajeros').val()==''){
        alert("Debe ingresar la cantidad de Pasajeros Correctamente, verifique e intente nuevamente.");
        $('#Pasajeros').focus();
        return false;
    }else{
        $('#buscando').show();
    }
}

function validarTerreno()    {
    if ($('#Terreno').val()==''){
        alert("Debe ingresar un tipo de Terreno, verifique e intente nuevamente.");
        $('#Terreno').focus();
        return false;
    }else{
        $('#buscando').show();
    }
}

function reload_page(){
    $('#buscando').show();
    location.reload();
}
