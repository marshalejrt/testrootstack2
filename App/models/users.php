<?php


class Users
{
    function Users()
    {
        $this->url='https://randomuser.me/api/';
        $this->param_extra='&exc=registered&noinfo';
    }

    /**
     * Funcion para obtener usuarios desde el servidor segun los parametros nat y results
     * @param $nat
     * @param $results
     * @return string
     */
    function getUsers($nat,$results){
        $response = file_get_contents($this->url.'?nat='.$nat.'&results='.$results);
        return $response;
    }

    /**
     * Funcion para obtener usuarios desde el servidor con cantidades grandes y excluyendo parametros
     * extras
     * @param $nat
     * @param $results
     * @return string
     */
    function getUsersAll($nat,$results){
        $response = file_get_contents($this->url.'?nat='.$nat.'&results='.$results.$this->param_extra);
        return $response;
    }

    /**
     * Funcion para ordenar un array segun la llave establecida
     * @param $array
     * @param $orderBy
     * @return mixed
     */
    function orderArrayBy($array,$orderBy){
        $sortArray = array();

        foreach($array['results'] as $person){
            foreach($person['dob'] as $key=>$value){
                if(!isset($sortArray[$key])){
                    $sortArray[$key] = array();
                }
                $sortArray[$key][] = $value;
            }
        }

        $orderby = $orderBy;

        array_multisort($sortArray[$orderby],SORT_ASC,$array['results']);
        return $array;

    }

    /**
     * Funcion para validar si un usuario es mayor a la edad establecida
     * @param $array
     * @param $age
     * @return array
     */
    function getUserAboveAge($array,$age){

        foreach($array['results'] as $person){
            if ($person['dob']['age']>$age){
                return $person;
            }
        }
        return array();

    }

    /**
     * Funcion para verificar cual caracter se utiliza m�s en un array
     * @param $array
     * @return array
     */
    function getMostUsedChar($array){
        $names='';
//        Acumulamos los names
        foreach($array['results'] as $person){
            $names=$names.strtoupper($person['name']['first']);
        }
        //Ordenamos alfab�ticamente los nombres
        $stringParts = str_split($names);
        sort($stringParts);
        $names_order= implode('', $stringParts);
        // Recorremos cada car�cter de la cadena Nombres
        $array_char=array();
        for($i=0;$i<strlen($names_order);$i++)
        {
            //Obtenemos el Char Actual
            $char=substr($names_order,$i,1);
            if (empty($array_char)){
                //Si esta vacio el array le agregamos uno nuevo
                $array_char[$char]=1;
            }else{
                //Verificamos que no exista el char actual en el array
                if (array_key_exists($char, $array_char)){
                    //si existe sumamos la cantidad usada
                    $array_char[$char]++;
                }else{
                    //Agregamos el Char Actual al Array
                    $array_char[$char]=1;
                }
            }

        }
        //Ordenamos el arreglo de forma ascendente
        arsort($array_char);
        $char_most_used=array('char'=>array_keys($array_char)[0],'cant'=>json_encode(array_values($array_char)[0]));
        return $char_most_used;

    }

}