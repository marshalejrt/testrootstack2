<?php


class Planets
{
    function Planets()
    {
        $this->url_planets='https://swapi.co/api/planets/';
        $this->array_pages=array(1,2,3,4,5,6,7);
        $this->most_population=0;
        $this->planet_by_terrain=array();
    }


    /**
     * Funcion para obtener la lista de planetas segun el id de la pagina
     * @param $idPage
     * @return string
     */
    function getPlanets($idPage){
        $response = file_get_contents($this->url_planets.'?page='.$idPage);
        return $response;
    }

    /**
     * Funcion para obtener la lista de planetas segun el terreno establecido
     * @param $Terreno
     * @return array
     */
    function getPlanetByTerrain($Terreno){
        $this->most_population=0;
        $this->planet_by_terrain=array();
        foreach($this->array_pages as $page){
            //Buscamos los planetas por pagina
            $response=$this->getPlanets($page);
            $current_planets=json_decode($response,true);
            //Escaneamos los planetas y verificamos que cumplan los parametros
            foreach($current_planets['results'] as $planet){
                //Validamos que sea el tipo de terreno y guardamos el que mayor poblacion tenga
                if ($this->containsString($Terreno,$planet['terrain']) && intval($planet['population'])>=$this->most_population){
                    $this->most_population=intval($planet['population']);
                    $this->planet_by_terrain=$planet;
                }

            }


        }

        return $this->planet_by_terrain;
    }


    /**
     * Funcion para validar que un string est� dentro de una cadena
     * @param $strsearch
     * @param $string
     * @return bool
     */
    function containsString($strsearch, $string)
    {
        return strpos(strtolower($string), strtolower($strsearch)) !== false;
    }

    /**
     * Funcion para buscar una imagen en googleImage segun el parametro establecido
     * debuelve un string 'src="http://image.jpg/"'
     * @param $keySearch
     * @return string
     */
    function searchImagePlanet($keySearch){
        //Buscamos la imagen en google para mostrarla en el front
        $search_keyword=str_replace(' ','+',$keySearch);
        $newhtml =file_get_contents("https://www.google.com/search?q=".$search_keyword."&tbm=isch");
        $source_img='src="';
        $pos_inicial=strpos($newhtml,'src="');
        for($i=$pos_inicial+5;$i<strlen($newhtml);$i++){
            $source_img=$source_img.substr($newhtml,$i,1);

            if(substr($newhtml,$i,1)=='"'){
                break;
            }
        }

        return $source_img;
    }


}