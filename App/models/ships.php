<?php


class Ships
{
    function Ships()
    {
        $this->url_films='https://swapi.co/api/films/';
        $this->url_starships='https://swapi.co/api/starships/';
        $this->starships=array();
        $this->max_speed_starship=0;
        $this->fasted_starship=array();
    }


    /**
     * Funcion para obtener una saga segun el id de la misma
     * @param $idFilms
     * @return string
     */
    function getFilm($idFilms){
        $response = file_get_contents($this->url_films.$idFilms.'/');
        return $response;
    }

    /**
     * Funcion para obtener los datos de un starship
     * @param $idStarship
     * @return string
     */
    function getStarships($idStarship){
        $response = file_get_contents($this->url_starships.$idStarship.'/');
        return $response;
    }


    /**
     * Funcion para obtener la saga segun los parametros establecidos
     * @param $arrayFilms
     * @return array
     */
    function getStarshipFilms($arrayFilms){
        //Vaciamos el Array de starsthips
        $this->starships=array();
        foreach($arrayFilms as $film){
            //Obtenemos la saga
            $response = $this->getFilm($film);
            $response = json_decode($response,true);
            //Capturamos las starships que le pertenecen
            foreach($response['starships'] as $star_ship){
                //Agregamos al array starships sin repetirlos
                if(array_search(basename($star_ship), $this->starships)==false){
                    array_push($this->starships,basename($star_ship));
                }
            }

        }
        return $this->starships;
    }


    /**
     * Funcion para obtener la nave m�s rapida segun los parametros y cantidad de pasajeros
     * @param $Array_Starships
     * @param $Pasajeros
     * @return array
     */
    function getFastestShip($Array_Starships,$Pasajeros){
        $this->max_speed_starship=0;
        $this->fasted_starship=array();
        foreach($Array_Starships as $starships){
            $response=$this->getStarships($starships);
            $current_starship=json_decode($response,true);
            if ($this->GetDaysConsumables($current_starship['consumables'])>=7 && intval($current_starship['passengers'])>=$Pasajeros){
                if(intval($current_starship['max_atmosphering_speed'])>=$this->max_speed_starship){
                    $this->max_speed_starship=$current_starship['max_atmosphering_speed'];
                    $this->fasted_starship=$current_starship;
                }
            }

        }
        return $this->fasted_starship;
    }


    /**
     * Funcion para calcular los d�as de viaje de una nave
     * @param $Consumable
     * @return int
     */
    function GetDaysConsumables($Consumable){
        if($Consumable=='unknown'){
            return 0;
        }
        switch (strtolower(explode(' ',$Consumable)[1])) {
            case 'days'  :
                return explode(' ',$Consumable)[0];
                break;
            case 'day'  :
                return explode(' ',$Consumable)[0];
                break;
            case 'month'  :
                return explode(' ',$Consumable)[0]*30;
                break;
            case 'months'  :
                return explode(' ',$Consumable)[0]*30;
                break;
            case 'year'  :
                return explode(' ',$Consumable)[0]*365;
                break;
            case 'years'  :
                return explode(' ',$Consumable)[0]*365;
                break;
        }
        return 0;

    }

}