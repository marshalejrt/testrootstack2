<html lang="en">
<?php
include_once('header.php');
if ((isset($_POST['Pasajeros']))) {
    include "../controllers/FastestShipController.php";
}


?>
<body>
    <?php
    include_once('menu.php');
    ?>
    <div class="container marketing">
        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Fastest Ship <span class="text-muted">Nave Mas Rapida</span></h2>

                <p class="lead">Usando https://swapi.co/ como fuente de data, crear function, que retornara un string:<br>
                    - Aceptara un argumento tipo entero, que indicara la cantidad de pasajeros requerida.<br>
                    - Debera calcular sobre todas los starships<br>
                    - Retornara el nombre la nave que coincida con los siguientes parametros:<br>
                    - Tiene la capacidad para transportar los pasajeros indicados<br>
                    - Puede viajar por al menos 1 semana.<br>
                    - Fue parte de la trilogia original (4, 5, 6)<br>
                    - Si mas de una nave coincide con dichos parametros, debera retornar la mas rapida.<br>
                </p>
            </div>
            <form action="#" method="post"  class="col-md-5">

                <div class="form-group">
                    <label for="Pasajeros">Pasajeros:</label>
                    <input type="number" value="<?php if ((isset($_POST['Pasajeros']))) {echo $_POST['Pasajeros'];}?>" min="1" class="form-control" id="Pasajeros" name="Pasajeros" placeholder="Pasajeros">
                </div>

                <input type="submit" value="Encontrar" class="btn btn-lg btn-primary" id="Encontrar" onclick="return validarPasajeros();" role="button"/>
                <strong class="d-inline-block mb-2 text-primary" style="display: none!important" id="buscando">Buscando Naves, por favor espere...</strong>

            </form>
        </div>

        <div class="row mb-2 users grid" name="users" id="users">
            <!--            {{--Listado de Usuarios--}}-->
            <?php
            if ((isset($_POST['Pasajeros']))) {
                if(empty($fastest_ship)){
                    echo '<h5>No se han encontrado una Nave con los Parametros establecidos</h5>';
                }else{
            ?>

                <div class="col-md-4 element-item">
                    <div class="card-container">
                        <div class="card">
                            <div class="front">
                                <div class="cover">
                                    <img src="../images/background.webp"/>
                                </div>
                                <div class="user">
                                    <img class="img-circle" src="../images/starships.webp" style="border-radius: 50%;"/>
                                </div>
                                <div class="content">
                                    <div class="main">
                                        <h6 class="name"><?php echo $fastest_ship['name']?></h6>
                                        <p class="profession"><?php echo $fastest_ship['manufacturer']?></p>
                                        <p class="text-justify">
                                            - Pasajeros: <?php echo $fastest_ship['passengers']?><br>
                                            - Tiempo de Viaje: <?php echo $fastest_ship['consumables']?><br>
                                            - Clase: <?php echo $fastest_ship['starship_class']?><br>
                                            - Hiperimpulsor: <?php echo $fastest_ship['hyperdrive_rating']?><br>
                                        </p>
                                    </div>
                                </div>
                            </div> <!-- end front panel -->
                            <div class="back">
                                <div class="header">
                                    <h5 class="motto">Modelo:<br><?php echo $fastest_ship['model']?> </h5>
                                </div>
                                <div class="content">
                                    <div class="main">
                                        <h4 class="text-center">Capacidad de Carga:</h4>
                                        <p class="text-center"><?php echo $fastest_ship['cargo_capacity']?></p>
                                        <h4 class="text-center">Personal:</h4>
                                        <p class="text-center"><?php echo $fastest_ship['crew']?></p>
                                        <h4 class="text-center">Velocidad:</h4>
                                        <p class="text-center"><?php echo $fastest_ship['max_atmosphering_speed']?></p>
                                    </div>
                                </div>
                            </div> <!-- end back panel -->
                        </div> <!-- end card -->
                    </div> <!-- end card-container -->
                </div>
            <?php
                }
            }
            ?>
        </div>


        <!-- /END THE FEATURETTES -->
<!--        <hr class="featurette-divider">-->

    </div>

    <div class="container">
        <?php
        include_once('footer.php');
        ?>
    </div>
</body>
</html>