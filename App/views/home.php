<html lang="en">
<?php
include_once('header.php');
?>
<body>
    <?php
    include_once('menu.php');
    ?>
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Prueba Pr&aacute;ctica para Rootstack</h1>
        <p class="">Crear un repositorio en Github / Gitlab o Bitbucket. Debe ser un repositorio privado.<br>
            - El c&oacute;digo debe poder ser ejecutado con versiones vanilla del lenguaje a utilizar.<br>
            - Seguir est&aacute;ndares de c&oacute;digo como si se fuera entregar a un cliente.<br>
            - Simplicidad en la l&oacute;gica utilizada.</p>
    </div>

    <div class="container">
        <div class="card-deck mb-3 text-center">
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Fetch & Order</h4>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled mt-3 mb-4">
                        <p>Usando <a href="https://randomuser.me/" target="_blank">https://randomuser.me/</a> como fuente de data, crear una funcion que retorne un arreglo:</p>
                        <li>- Debe retornar 10 personas.</li>
                        <li>- Las personas deberan estar ordenadas por nombre.</li>
                    </ul>
                    <a  href="FetchOrder.php" class="btn btn-lg btn-block btn-outline-primary">Probar</a>
                </div>
            </div>
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Fetch & Find</h4>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled mt-3 mb-4">
                        <p>Usando <a href="https://randomuser.me/" target="_blank">https://randomuser.me/</a> como fuente de data, crear function, que acepte como
                            argumento una edad y retorne la data de la persona:</p>
                        <li>- Retornara 1 sola persona.</li>
                        <li>- La persona a retornar sera mayor de la edad especificada como argumento.</li>
                    </ul>
                    <a  href="FetchFind.php" class="btn btn-lg btn-block btn-outline-primary">Probar</a>
                </div>
            </div>
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Fetch & Count</h4>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled mt-3 mb-4">
                        <p>Usando <a href="https://randomuser.me/" target="_blank">https://randomuser.me/</a> como fuente de data, crear function, que retornara un
                            char/string:</p>
                        <li>- Debera obtener 5 personas.</li>
                        <li>- En base a los nombres debera calcular cual es la letra mas utilizada.</li>
                    </ul>
                    <a  href="FetchCount.php" class="btn btn-lg btn-block btn-outline-primary">Probar</a>
                </div>
            </div>
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Fastest Ship</h4>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled mt-3 mb-4">
                        <p>Usando <a href="https://swapi.co/" target="_blank">https://swapi.co/</a> como fuente de data, crear function, que retornara un string:</p>
                        <li>- Aceptara un argumento tipo entero, que indicara la cantidad de pasajeros requerida.</li>
                        <li>- Debera calcular sobre todas los starships</li>
                        <li>- Retornara el nombre la nave que coincida con los siguientes parametros:</li>
                        <li>- Tiene la capacidad para transportar los pasajeros indicados</li>
                        <li>- Puede viajar por al menos 1 semana.</li>
                        <li>- Fue parte de la trilogia original (4, 5, 6)</li>
                        <li>- Si mas de una nave coincide con dichos parametros, debera retornar la mas rapida.</li>

                    </ul>
                    <a  href="FastestShip.php" class="btn btn-lg btn-block btn-outline-primary">Probar</a>
                </div>
            </div>
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Planet by Terrain</h4>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled mt-3 mb-4">
                        <p>Usando <a href="https://swapi.co/" target="_blank">https://swapi.co/</a> como fuente de data, crear function, que retornara un string:</p>
                        <li>- Aceptara un argumento tipo string, que indicara el tipo de terreno.</li>
                        <li>- Retornara el nombre del planeta que coincida con los siguientes parametros:</li>
                        <li>- Coincide el tipo de terreno especificado como parametro.</li>
                        <li>- Si mas de un planeta coincide con dichos parametros, debera retornar el que posea mas
                            poblacion.</li>
                    </ul>
                    <a  href="PlanetTerrain.php" class="btn btn-lg btn-block btn-outline-primary">Probar</a>
                </div>
            </div>


        </div>
        <?php
        include_once('footer.php');
        ?>
    </div>
</body>
</html>