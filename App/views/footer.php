<footer class="pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
        <div class="col-12 col-md">
            <img itemprop="logo" src="https://www.rootstack.com/img/logo.png" alt="Rootstack" class="logo">
            <small class="d-block mb-3 text-muted">Realizado por <a href="http://indexsoftwareca.com/ejdev/" target="_blank">Ing. Efrain Rodriguez</a></small>
            <small class="d-block mb-3 text-muted">Para <a href="https://www.rootstack.com/" target="_blank">Rootstack</a></small>
            <small class="d-block mb-3 text-muted">Octubre 2019</small>
        </div>
        <div class="col-6 col-md">
            <h5>Ejercicios</h5>
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="FetchOrder.php">Fetch & Order</a></li>
                <li><a class="text-muted" href="FetchFind.php">Fetch & Find</a></li>
                <li><a class="text-muted" href="FetchCount.php">Fetch & Count</a></li>
                <li><a class="text-muted" href="FastestShip.php">Fastest Ship</a></li>
                <li><a class="text-muted" href="PlanetTerrain.php">Planet by Terrain</a></li>
            </ul>
        </div>
    </div>
</footer>
<script src="../js/UsersFunctions.js?r=<?= date('d-m-Y H:i:s') ?>"></script>