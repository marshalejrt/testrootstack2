<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Efrain Rodriguez">
    <title>Test Rootstack</title>

    <script src="../js/jquery.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="../css/rotating-card.css" rel="stylesheet" />
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <script src="https://getbootstrap.com/docs/4.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>

    <!-- Custom styles for this template -->
    <link href="../css/styles.css" rel="stylesheet">
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" />
    <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
</head>