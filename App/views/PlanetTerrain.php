<html lang="en">
<?php
include_once('header.php');
if ((isset($_POST['Terreno']))) {
    include "../controllers/PlanetTerrainController.php";
}


?>
<body>
    <?php
    include_once('menu.php');
    ?>
    <div class="container marketing">
        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Planet by Terrain <span class="text-muted">Terrenos Planetarios</span></h2>

                <p class="lead">Usando https://swapi.co/ como fuente de data, crear function, que retornara un string:<br>
                    - Aceptara un argumento tipo string, que indicara el tipo de terreno.<br>
                    - Retornara el nombre del planeta que coincida con los siguientes parametros:<br>
                    - Coincide el tipo de terreno especificado como parametro.<br>
                    - Si mas de un planeta coincide con dichos parametros, debera retornar el que posea mas<br>
                    poblacion.<br>

                </p>
            </div>
            <form action="#" method="post"  class="col-md-5">

                <div class="form-group">
                    <label for="Terreno">Terreno:</label>
                    <input value="<?php if ((isset($_POST['Terreno']))) {echo $_POST['Terreno'];} ?>" min="1" class="form-control" id="Terreno" name="Terreno" placeholder="rainforests, rivers, mountains,jungles, forests, deserts, etc">
                </div>

                <input type="submit" value="Encontrar" class="btn btn-lg btn-primary" id="Encontrar" onclick="return validarTerreno();" role="button"/>
                <strong class="d-inline-block mb-2 text-primary" style="display: none!important" id="buscando">Buscando Planetas, por favor espere...</strong>

            </form>
        </div>

        <div class="row mb-2 users grid " name="users" id="users">
            <!--            {{--Listado de Usuarios--}}-->
            <?php
            if ((isset($_POST['Terreno']))) {
                if(empty($planet_by_terrain)){
                    echo '<h5>No se han encontrado un Planeta con los Parametros establecidos</h5>';
                }else{
            ?>

                <div class="col-md-4 element-item">
                    <div class="card-container">
                        <div class="card">
                            <div class="front">
                                <div class="cover">
                                    <img <?php echo $src_planet_background?>/>
                                </div>
                                <div class="user">
                                    <img class="img-circle" <?php echo $src_planet?> style="border-radius: 50%;"/>
                                </div>
                                <div class="content">
                                    <div class="main">
                                        <h6 class="name"><?php echo $planet_by_terrain['name']?></h6>
                                        <p class="profession">Poblacion: <?php echo $planet_by_terrain['population']?></p>
                                        <p class="text-justify">
                                            - Terreno: <?php echo $planet_by_terrain['terrain']?><br>
                                            - Periodo de Rotacion: <?php echo $planet_by_terrain['rotation_period']?><br>
                                            - Periodo de Orbita: <?php echo $planet_by_terrain['orbital_period']?><br>
                                            - Diametro: <?php echo $planet_by_terrain['diameter']?><br>
                                        </p>
                                    </div>
                                </div>
                            </div> <!-- end front panel -->
                            <div class="back">
                                <div class="header">
                                    <h5 class="motto">Clima:<br><?php echo $planet_by_terrain['climate']?> </h5>
                                </div>
                                <div class="content">
                                    <div class="main">
                                        <h4 class="text-center">Gravedad:</h4>
                                        <p class="text-center"><?php echo $planet_by_terrain['gravity']?></p>
                                        <h4 class="text-center">Superficie del agua:</h4>
                                        <p class="text-center"><?php echo $planet_by_terrain['surface_water']?></p>
                                    </div>
                                </div>
                            </div> <!-- end back panel -->
                        </div> <!-- end card -->
                    </div> <!-- end card-container -->
                </div>
            <?php
                }
            }
            ?>
        </div>


        <!-- /END THE FEATURETTES -->
<!--        <hr class="featurette-divider">-->

    </div>

    <div class="container">
        <?php
        include_once('footer.php');
        ?>
    </div>
</body>
</html>