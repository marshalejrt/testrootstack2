<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal">Rootstack</h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="home.php">Inicio</a>
        <a class="p-2 text-dark" href="FetchOrder.php">Fetch & Order</a>
        <a class="p-2 text-dark" href="FetchFind.php">Fetch & Find</a>
        <a class="p-2 text-dark" href="FetchCount.php">Fetch & Count</a>
        <a class="p-2 text-dark" href="FastestShip.php">Fastest Ship</a>
        <a class="p-2 text-dark" href="PlanetTerrain.php">Planet by Terrain</a>
    </nav>
    <a class="btn btn-outline-primary" href="http://indexsoftwareca.com/ejdev/" target="_blank">Ir a mi Web</a>
</div>
<!--End Menu-->
