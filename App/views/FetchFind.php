<html lang="en">
<?php
include_once('header.php');
if ((isset($_POST['Edad']))) {
    include "../controllers/FetchFindController.php";
}


?>
<body>
    <?php
    include_once('menu.php');
    ?>
    <div class="container marketing">
        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Fetch & Find <span class="text-muted">Obtener y Encontrar</span></h2>

                <p class="lead">Usando https://randomuser.me/ como fuente de data, crear function, que acepte como
                    argumento una edad y retorne la data de la persona:<br>
                    - Retornara 1 sola persona.<br>
                    - La persona a retornar sera mayor de la edad especificada como argumento.</p>
            </div>
            <form action="#" method="post"  class="col-md-5">

                <div class="form-group">
                    <label for="Edad">Edad:</label>
                    <input type="number" value="<?php if ((isset($_POST['Edad']))) {echo $_POST['Edad'];}?>" min="1" class="form-control" id="Edad" name="Edad" placeholder="Edad">
                </div>

                <input type="submit" value="Encontrar" class="btn btn-lg btn-primary" id="Encontrar" onclick="return validarEdad();" role="button"/>
                <strong class="d-inline-block mb-2 text-primary" style="display: none!important" id="buscando">Buscando Usuarios...</strong>

            </form>
        </div>

        <div class="row mb-2 users grid" name="users" id="users">
            <!--            {{--Listado de Usuarios--}}-->
            <?php
            if ((isset($_POST['Edad']))) {
                if(empty($user_above_age)){
                    echo '<h5>No se han encontrado un Usuario mayor a la edad establecida</h5>';
                }else{
            ?>

                <div class="col-md-4 element-item">
                    <div class="card-container">
                        <div class="card">
                            <div class="front">
                                <div class="cover">
                                    <img src="<?php echo $user_above_age['picture']['large'] ?>" style="width: 800%;"/>
                                </div>
                                <div class="user">
                                    <img class="img-circle" src="<?php echo $user_above_age['picture']['large'] ?>" style="border-radius: 50%;"/>
                                </div>
                                <div class="content">
                                    <div class="main">
                                        <h6 class="name"><?php echo $user_above_age['name']['first']?> <?php echo ' '. $user_above_age['name']['last'] ?></h6><div class="last_name"><?php echo ' '. $user_above_age['name']['last'] ?></div>
                                        <p class="profession"><?php echo $user_above_age['email']?></p>
                                        <p class="text-center">
                                            "I'm from
                                            <?php echo
                                                $user_above_age['location']['street']['name'] . ' # '.
                                                $user_above_age['location']['street']['number'] .', ' .
                                                $user_above_age['location']['city'] .', State of ' .
                                                $user_above_age['location']['state'] .', '.
                                                $user_above_age['location']['country']
                                            ?> "
                                        </p>
                                    </div>
                                </div>
                            </div> <!-- end front panel -->
                            <div class="back">
                                <div class="header">
                                    <h5 class="motto">"My phone is <?php echo $user_above_age['phone']?>"</h5>
                                </div>
                                <div class="content">
                                    <div class="main">
                                        <h4 class="text-center">I'm <?php echo $user_above_age['dob']['age']?> years old</h4>
                                        <p class="text-center">Username:<br><?php echo $user_above_age['login']['username']?></p>
                                    </div>
                                </div>
                                <div class="footer">
                                    <div class="social-links text-center">
                                        <a href="#" class="facebook"><i class="fa fa-facebook fa-fw"></i></a>
                                        <a href="#" class="google"><i class="fa fa-google-plus fa-fw"></i></a>
                                        <a href="#" class="twitter"><i class="fa fa-twitter fa-fw"></i></a>
                                    </div>
                                </div>
                            </div> <!-- end back panel -->
                        </div> <!-- end card -->
                    </div> <!-- end card-container -->
                </div>
            <?php
                }
            }
            ?>
        </div>


        <!-- /END THE FEATURETTES -->
<!--        <hr class="featurette-divider">-->

    </div>

    <div class="container">
        <?php
        include_once('footer.php');
        ?>
    </div>
</body>
</html>