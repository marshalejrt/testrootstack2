<html lang="en">
<?php
include_once('header.php');
include "../controllers/FetchOrderController.php";


?>
<body>
    <?php
    include_once('menu.php');
    ?>
    <div class="container marketing">
        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Fetch & Order <span class="text-muted">Obtener y ordenar</span></h2>

                <p class="lead">Usando https://randomuser.me/ como fuente de data, crear una funcion que retorne un
                    arreglo:<br>
                    - Debe retornar 10 personas.<br>
                    - Las personas deberan estar ordenadas por nombre</p>
            </div>
            <div class="col-md-5">
                <p><a class="btn btn-lg btn-primary " href="#" onclick="reload_page();" name="btn_reload" id="btn_reload"
                      role="button">Cargar de Nuevo</a> <strong class="d-inline-block mb-2 text-primary" style="display: none!important" id="buscando">Buscando Usuarios...</strong></p>
                <h6>Ordenar Por:</h6>
                <div class="btn-group btn-group-lg sort-by-button-group" role="group" aria-label="Large button group">
                    <button type="button" class="btn btn-secondary original-order" data-sort-by="original-order">Original</button>
                    <button type="button" class="btn btn-secondary name-order" data-sort-by="name">Nombre</button>
                    <button type="button" class="btn btn-secondary last_name-order" data-sort-by="last_name">Apellido</button>
                </div>

            </div>
        </div>

        <div class="row mb-2 users grid" name="users" id="users">
            <!--            {{--Listado de Usuarios--}}-->
            <?php
            foreach ($list_users['results'] as $k => $v) {
            ?>
                <div class="col-md-4 element-item">
                    <div class="card-container">
                        <div class="card">
                            <div class="front">
                                <div class="cover">
                                    <img src="<?php echo $v['picture']['large'] ?>" style="width: 800%;"/>
                                </div>
                                <div class="user">
                                    <img class="img-circle" src="<?php echo $v['picture']['large'] ?>" style="border-radius: 50%;"/>
                                </div>
                                <div class="content">
                                    <div class="main">
                                        <h6 class="name"><?php echo $v['name']['first']?> <?php echo ' '. $v['name']['last'] ?></h6><div class="last_name"><?php echo ' '. $v['name']['last'] ?></div>
                                        <p class="profession"><?php echo $v['email']?></p>
                                        <p class="text-center">
                                            "I'm from
                                            <?php echo
                                                $v['location']['street']['name'] . ' # '.
                                                $v['location']['street']['number'] .', ' .
                                                $v['location']['city'] .', State of ' .
                                                $v['location']['state'] .', '.
                                                $v['location']['country']
                                            ?> "
                                        </p>
                                    </div>
                                </div>
                            </div> <!-- end front panel -->
                            <div class="back">
                                <div class="header">
                                    <h5 class="motto">"My phone is <?php echo $v['phone']?>"</h5>
                                </div>
                                <div class="content">
                                    <div class="main">
                                        <h4 class="text-center">I'm <?php echo $v['dob']['age']?> years old</h4>
                                        <p class="text-center">Username:<br><?php echo $v['login']['username']?></p>
                                    </div>
                                </div>
                                <div class="footer">
                                    <div class="social-links text-center">
                                        <a href="#" class="facebook"><i class="fa fa-facebook fa-fw"></i></a>
                                        <a href="#" class="google"><i class="fa fa-google-plus fa-fw"></i></a>
                                        <a href="#" class="twitter"><i class="fa fa-twitter fa-fw"></i></a>
                                    </div>
                                </div>
                            </div> <!-- end back panel -->
                        </div> <!-- end card -->
                    </div> <!-- end card-container -->
                </div>
            <?php
            }
            ?>
        </div>


        <!-- /END THE FEATURETTES -->

    </div>

    <div class="container">
        <?php
        include_once('footer.php');
        ?>
    </div>

</body>
</html>